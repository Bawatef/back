-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 09 Mars 2018 à 11:07
-- Version du serveur :  10.0.34-MariaDB-0ubuntu0.16.04.1
-- Version de PHP :  7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `referentiel`
--

-- --------------------------------------------------------

--
-- Structure de la table `ref`
--

CREATE TABLE `ref` (
  `id` int(55) NOT NULL,
  `client` varchar(255) NOT NULL,
  `context` text NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Publish',
  `objective` text NOT NULL,
  `presentation` text NOT NULL,
  `outputref` text NOT NULL,
  `createdAT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `ref`
--

INSERT INTO `ref` (`id`, `client`, `context`, `status`, `objective`, `presentation`, `outputref`, `createdAT`) VALUES
(1, 'La banque postale', 'En tant que développeur de produits Open Source comme Linshare, OBM ou LinKPI, nous pouvons vous aider à réaliser vos projets d’intégration autour de cette suite logicielle.\r\nEn tant qu’éditeur, nous contrôlons la conception, le développement, et le support, et nous vous aidons à sécuriser le déploiement du logiciel au sein de votre organisation.', 'Publish', 'L’objectif de ce type d’étude est de fournir une évaluation globale et à jour du logiciel libre pour une technologie donnée. Cette étude se présente de la forme suivante:', 'Nous sommes tous passionnés. Nous aimons tous vous aider dans la construction de votre projet Open Source.\r\nParfois, la meilleure chose à faire est de collaborer directement avec l’équipe de notre client. C’est pourquoi LINAGORA propose une expertise interne: nos nombreux experts étant contributeurs dans les commauntés comme LibreOffice, LimeSurvey, GanttProject ou Drupal, vous êtes sûrs d’avoir la bonne personne en face de vous pour vous aider.', 'Cette approche permet aux clients de mettre en place des infrastructures et applications qui combinent les dernières avancées dans le logiciel Open Source et la meilleure qualité possible de l’intégration et du support.', '2018-03-09 09:44:22'),
(2, 'Médecins du Monde', 'Médecins du Monde (ONG) - Association indépendante, Médecins du Monde agit au-delà du soin. Elle dénonce les atteintes à la dignité et aux droits de l\'homme et se bat pour améliorer la situation des populations. Dans le cadre de sa transformation digitale globale, l\'ONG Médecins du Monde désire réaliser la refonte de son site internet. Le projet global était piloté par DDB.', 'Publish', 'L\'objectif est de véhiculer une image « actuelle » : système de navigation moderne accessible depuis un ensemble de terminaux très divers. Il s\'agit de réaliser un accompagnement à la fois technique et méthodologique.', 'Le projet a consisté à : - Réaliser les spécifications techniques de la plate-forme ; - Assurer les développements techniques HTML du site ; - Assurer l\'intégration du site.', 'Précisions sur le site web : - Site multi langues. - Responsive design : 2 points de rupture seront implémentés sur le site. - Compatible avec IE 9, 10, 11 ; FF, Chrome et Android 4. - Formulaire de contact multi-destinataire. - Inscription à des évènements avec compteur et email de remerciement (création/affichage d\'évènement, formulaire d\'inscription). - Module de cartographie avec affichage de puces sur les différents pays ou régions dans lesquels l\'association intervient. - Mise en place de pages interfacées avec des Webservices de la solution « profilsoft » pour la création d\'un section « offre d\'emploi ».', '2018-03-09 09:45:55'),
(3, 'SNCF', 'L\'un des plus anciens site de e-commerce français, basé J2EE\r\nBeaucoup d\'interactions avec des services tiers\r\nRobustesse et scalabilité sont des éléments fondamentaux\r\nUn traffic de plus en plus important sur leur site web', 'Publish', 'Sélectionner un framework ou une technologie qui permet la robustesse à grande échelle.\r\nPreserver les fonctionnalités et la structuration.\r\nPréserver et prolonger les possibilités d\'intégration avec des tiers (incluant des réseaux sociaux).', 'Un CMS de renom possédant une communauté active unie et une documentation riche.\r\nL\'extensibilité du framework Drupal permet à Voyages-SNCF d\'évoluer en temps réel\r\nUne meilleure compatibilité avec les navigateurs ', 'Voyages-SNCF fait parti des plus anciens site Web français de commerce. Tandis que son but initial était seulement de vendre des billets de train, il  s’est finalement transformé en plate-forme de voyage majeure. Avec une clientèle toujours plus grande et des partenariats avec des hôtels, des compagnies aériennes et des taxis, la plate-forme initiale s\'est approchée de ses limites.', '2018-03-09 09:51:11'),
(4, 'Ministère de la Défense', 'Variété de logiciels libres\r\nExigences de haute disponibilité', 'Publish', 'Temps de réponse rapide pour les problèmes de blocage\r\nAssistance aux personnes utilisant des logiciels libres', 'Garantie de disponibilité en continu\r\nLa satisfaction de l\'utilisateur avec des logiciels libres\r\nExpansion potentielle de la stratégie de logiciel libre à d\'autres parties de l\'infrastructure', 'Maintenance corrective avec 4 heures maximum de temps réponse sur les problèmes de blocage\r\nAssistance de l\'utilisateur', '2018-03-09 09:53:38'),
(5, 'Région Ile de France', 'Beaucoup d\'interactions avec des services tiers\r\nRobustesse et scalabilité sont des éléments fondamentaux\r\nUn traffic de plus en plus important sur leur site web', 'Publish', 'Installer une infrastructure web permettant le déploiement rapide de sites satellites.\r\nCréer une interface intuitive afin d\'encourager les contributions de contenu\r\nConcevoir un portail qui combine richesse d\'informations et facilité de navigation', 'Design de l\'architecture technique basé sur Drupal.\r\nCréation d\'une distribution spécifique pour le déploiement rapide de sites supplémentaires.\r\nDéveloppement de modules adaptés qui seront contribués en arrière à la communauté.\r\nConfiguration et optimisation de la plate-forme pendant l\'exécution.', 'La distribution Drupal permet le déploiement facile et rapide de sites spécifiques.\r\nUne conception intelligente qui améliore l\'accès à l\'information.\r\nDes configurations adaptées qui encouragent les contributions.\r\nL\'architecture assure la disponibilité haute malgré le trafic dense.', '2018-03-09 09:55:20'),
(6, 'ACOSS', 'ACOSS fédère plus de100 entités pour collecter les paiements de la Sécurité Sociale\r\nPlus de 6 millions de cotisants\r\nDes centaines de milliards d\'euros recouvrés\r\nBesoin d\'une infrastructure de collecte pour un nouveau réseau de 400 partenaires', 'Publish', 'Définir une SOA pour préparer l\'intégration prochaine de nouveaux partenaires\r\nChoisir une technologie flexible et standard pour une adaption rapide à n\'importe quel outil\r\nAssurer l\'adaptabilité et la solidité de l\'infrastructure\r\nÉviter à ACOSS de dépendre des logiciels propiétaires', 'Définition d\'un SOA adapté sur Petals ESB\r\nConfiguration et déploiement de Petals ESB\r\nSupport et évolutions', 'Temps d\'execution en continu pendant des mois\r\nDes millions de messages échangés au quotidien\r\nJBI-compliant ESB pour faciliter les évolutions et l\'intégration\r\nProduit 100% Open Source: pas de coûts cachés, pas de verrou de l\'éditeur', '2018-03-09 09:56:56'),
(7, 'Défenseur des Droits', 'En moyenne 1000 visiteurs sur le portail\r\nLes utilisateurs devaient faire face à plus de 200 domaines différents\r\nLes capacités de recherche étaient jugées insuffisantes pour trouver des informations\r\nToute adaptation de l\'extranet était un challenge', 'Publish', 'Améliorer l\'efficacité des recherches\r\nRendre la complexité des flux de travail et de domaine aussi claire que possible aux conseillers juridiques\r\nFaciliter l\'utilisation de l\'interface\r\nRendre l\'extranet maintenable et adaptable par l\'équipe interne.', 'Redéfinir les modèles de données, les affichages et flux de travail dans des fichiers YAML.\r\nDéployer et configurer un moteur de recherche avancé SolR et l\'extracteur de metadata TIKA.\r\nIntégrer LemondLDAP pour fournir une authentification unique (SSO) à tous\r\nConcevoir des interfaces utilisateur', 'L\'administration gagne de l\'autonomie concernant le cycle de vie de l\'extranet\r\nLa complexité des données est maintenant réduite grâce à un système de recherche flexible\r\nL\'intégration de LemonLDAP facilite la gestion de plusieurs applications par l\'utilisateur.\r\nLes flux de travail sont configurés dans l\'extranet pour faciliter la surveillance de l\'avancement.', '2018-03-09 09:58:26'),
(8, 'SDIS 59', '150 bureaux et casernes de pompiers couvrant la zone\r\nEnviron 10000 pompiers à contacter et coordonner\r\nDes besoins critiques de circulation rapide et fiable de l\'information', 'Publish', 'Permettre un partage facile de l\'information, améliorer la planification et le suivi\r\nFaciliter la coopération entre les différentes casernes grâce à un répertoire d\'utilisateurs commun\r\nObtenir une plateforme solide pour assurer un service continu', 'Configuration et déploiement de la plate-forme de collaboration OBM\r\nDéploiement et intégration de Linshare, une solution facile de partage de fichiers\r\nInstallation de la solution Open Source LinID, pour une gestion des idéntités et des accès\r\nConfiguration de la gestion de listes d\'emails avec Sympa', 'Gestion d\'utilisateur simplifiée (permissions et accès au contenu)\r\nSolutions d\'intégration qui fournissent un portail simple et unique\r\nPartage puissant avec aucune limite de taille et un contrôle précis des accès\r\nPlateforme solide, libre et extensible', '2018-03-09 09:59:56');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'mohamed', 'tunis'),
(2, 'majda', 'sfax'),
(3, 'awatef', 'linagora'),
(4, 'iwan', 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `userss`
--

CREATE TABLE `userss` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `userss`
--

INSERT INTO `userss` (`id`, `username`, `password`) VALUES
(28, 'awatef', '51553d5c0ff6e62f284d26eef266145cea811f25'),
(32, 'Admin', '51553d5c0ff6e62f284d26eef266145cea811f25');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `ref`
--
ALTER TABLE `ref`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `userss`
--
ALTER TABLE `userss`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `ref`
--
ALTER TABLE `ref`
  MODIFY `id` int(55) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `userss`
--
ALTER TABLE `userss`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
