
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="favicon.ico">

    <title>Références - Linagora</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/narrow-jumbotron.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/styless.css">
</head>

<body>


      <header class="header clearfix"> <!-- header -->
          <h1 class="text-center"> Ajouter un nouveau client </h1>
      </header>

<main class="mainadd">

  <div class="nav">
  <div class="user">
  <?php
  session_start();
  echo "Bonjour ".$_SESSION['username'];
  //}
  ?>
  </div>
  <div class="deconnexion">
        <form  action= 'index.php'>
          <input type="submit"  value="Déconnexion">
        </form>
  </div>
  <div class="retour">
  <form  action= 'references.php'>
            <div class="update">
            <input type="submit"  value="Retour">
           </div>
  </form>
  </div>
  </div>

  <div class="row">
  <form action="add.php" method="get" class="form-horizontal">
            <div class="col-lg-12">
                    <h4>Client</h4>
                    <input type="text" class='form-control' id='client' name='client' placeholder="Client" required>
            </div>

            <div class="col-lg-6">
                <h4>Context</h4>
                <textarea type="text" class='form-control' id='context' name='context' required></textarea>

                <h4>Project presentation</h4>
                <textarea type="text" class='form-control' id='presentation' name='presentation' required></textarea>
            </div>

            <div class="col-lg-6">
                <h4>Objective</h4>
                <textarea type="text" class='form-control' id='objective' name='objective' required></textarea>

                <h4>Output</h4>
                <textarea type="text" class='form-control' id='outputref' name='outputref' required></textarea>
            </div>

            <div class="add">
                <input type="submit" value="Add reference">
            </div>
  </form>
</div>

<?php
require('database.php');
//$inser = $con->prepare("SELECT * FROM `ref` ");
$inser = $con->prepare("INSERT INTO `ref` (`client`, `context`, `objective`, `presentation`, `outputref`) VALUES (:client, :context, :presentation, :objective, :outputref)");
$inser->execute(array(
    'client' =>$_GET['client'],
  'context'  =>$_GET['context'],
   'presentation'=> $_GET["presentation"],
   'objective'=> $_GET["objective"],
   'outputref'=> $_GET["outputref"],

  ));
//action='references_test.php'
//header('Location:http://localhost/added.php');
//$test = array_filter($_GET);
//$nbChampsRemplis = count($test);
//echo $nbChampsRemplis;
//echo $_GET['client'] ;
?>




</main>
</body>
</html>
