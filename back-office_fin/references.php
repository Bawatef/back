<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="favicon.ico">

    <title>Références - Linagora</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/narrow-jumbotron.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" href="css/styless.css" media="all">
</head>

<body>
 <!-- container -->


    <header class="header">
        <h1 class="text-center"> Références Linagora </h1>
    </header> <!-- /header -->

  <main class = "main"> <!-- main -->
  <!-- affichage du nom d'utilisateur-->

  <div class="nav">
  <div class="user">
  <?php
  session_start();
  echo "Bonjour ".$_SESSION['username'];
  //}
  ?>
  </div>
  <form method="get" action="search.php" class="search">
          <input type="search" placeholder="Rechercher" name="clef" id="lookup" />
          <button type="submit" class="icone-loupe">Q</button>
  </form>

  <div class="deconnexion">
        <form  action= 'index.php'>
          <input type="submit"  value="Déconnexion">
        </form>
  </div>
  <div class="new">
    <a href='add.php?id=".$row['id']."' class='btn btn-warning'>Add new</a>
    </div>

  </div>
  <!-- /////////////////////////// tableau //////////////////////////// -->
 <div class="table-responsive">
  <?php
  require('database.php');
  $query = "SELECT * FROM ref";
  //Parcours et affiches les noms des colonnes
  print "<table> ";
  $result = $con->prepare($query);
  //recupère uniquement la 1ere ligne (on a besoin uniquement des nom de champs)
$result ->execute();
$row = $result->fetch(PDO::FETCH_ASSOC);
//récupérer l'entete de la table//
print "<thead> ";
print " <tr> ";
foreach ($row as $field => $value){
    print " <th scope='col'>$field</th> ";
} // end foreach
print " </tr> ";
print "</thead> ";

//récupérer le corp de la table//

$data = $con->query($query);
$data->setFetchMode(PDO::FETCH_ASSOC);


  foreach($data as $row){
      print " <thead> ";
      print " <tbody> ";
      foreach ($row as $name=>$value){
          print substr(" <td> $value </td> ", 1, 50);
      } // end field loop

      print "<td><a href='view.php?id=".$row['id']."'class='btn btn-primary'>View</a></td>";

      print "<td><a href='edit.php?id=".$row['id']."' class='btn btn-success'>Edit</a></td>";

      //$del = '?delete=' . $row['id'];
      //$_GET['delete'] = $del;
      print "<td><a href='delete.php?id=".$row['id']."' class='btn btn-danger' onclick='return confirm('Are you sure you want to delete this reference ?')'>Delete</a></td>";

  } // end record loop
  print "</thead>";
  print "</tbody>";
  //print "<tfoot> <td><a href='/back/back-office/add.php?id=".$row['id']."' class='btn btn-success'>New</a></td></tfoot>";
  print "</table>";


  // end try

  ?>


</main>
<!-- footer -->
    <footer class="footer">

    <div class="foot">
        <p> <br> &copy; Awatef la Reine du Code</p>
      </div>
    </footer> <!-- /footer -->

<!-- /container -->
</body>
</html>
