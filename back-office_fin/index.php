+<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Login - Référentiel</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">

    <!-- Import font-awesome -->
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/styless.css">
</head>

<body class="index">
<?php

/* /////////////////////////// CONNEXION BDD //////////////////////////// */

  require('database.php');

  $password = '';
  $condition = false;

  $select = $con->prepare('SELECT * FROM userss WHERE username = :username OR username = :createusername');

  $select->execute(array(
      'username' => $_GET['username'],
      'createusername' => $_GET['create_username'],
  ));

  /* /////////////////////////// RECUPERATION DONNEES //////////////////////////// */

  $results = $select->fetchAll();
  foreach ($results as $item) {
      $username = $item['username'];
      $password = $item['password'];
  }

  /* /////////////////////////// CONNEXION //////////////////////////// */

  if(!empty($_GET['username']) && ($_GET['password'])) {
    if (sha1($_GET['password']) === $password) {
      session_start();
     $_SESSION['username']=$_GET['username'];
      header('Location:http://localhost//back/back-office/references.php');
      die();
      echo 'Bon mot de passe';
    } else {
      echo 'Mauvais mot de passe';
    }
  }
 /* ////// session */

  /* /////////////////////////// CREATION DE COMPTE //////////////////////////// */

  if($_GET['create_password'] === $_GET['create_password2']) {

    if($_GET['create_username'] != $username) {

      $insert = $con->prepare("INSERT INTO `userss` (`username`, `password`) VALUES (:username, :password)");
      $insert->execute(array(
        'username' => $_GET['create_username'],
        'password' => sha1($_GET['create_password']),
      ));

      $condition = true;
      echo 'Création du compte';

    } elseif($condition = false) {
      echo 'Le compte est déjà existant';
    }
  } else {
    echo 'Les deux mots de passe que vous avez rentrés ne correspondent pas…';
  }

?>

<main class="mainindex">
  <div class="login"> <!-- container -->
    <form class="form-signin" method="GET"> <!-- form -->
        <img class="logo" src="img/logo-linagora.png" alt="logo-linagora.png">
        <div class="input">
            <input type="text" name="username" class="form-control" placeholder="Username" required autofocus>
            <label class="icon-right" for="inputUsername">
                <i class="fa fa-paper-plane"></i>
            </label>
        </div>


        <div class="input">
            <input type="password" name="password" class="form-control" placeholder="Mot de passe" required>
            <label class="icon-right" for="inputPassword">
                <i class="fa fa-lock" aria-hidden="true"></i>
            </label>

        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Connection <i class="fa fa-sign-in" aria-hidden="true"></i></button>
        <a href="references.php" class="btn btn-lg btn-primary btn-block">DEMO</a>
    </form> <!-- /form -->
</div>

<!-- /formulaire d'inscription -->

<div classe="inscription">

	<form class="form-signin" class= "boite" method="GET">
<div class="creer"> <h3> Créer un compte </h3> </div>
 <div class="input">
            <input type="text" name="create_username" class="form-control" placeholder="Username" required autofocus>
            <label class="icon-right" for="inputUsername">
                <i class="fa fa-paper-plane"></i>
            </label>
        </div>


        <div class="input">
            <input type="password" name="create_password" class="form-control" placeholder="Mot de passe" required>
            <label class="icon-right" for="inputPassword">
                <i class="fa fa-lock" aria-hidden="true"></i>
            </label>

        </div>

<div class="input">
            <input type="password" name="create_password2" class="form-control" placeholder="Confirmer Mot de passe" required>
            <label class="icon-right" for="inputPassword">
                <i class="fa fa-lock" aria-hidden="true"></i>
            </label>

        </div>


        <button class="btn btn-lg btn-primary btn-block" type="submit">M'inscrire <i class="fa fa-sign-in" aria-hidden="true"></i></button>
        <a href="index.php" class="btn btn-lg btn-primary btn-block">DEMO</a>
    </form> <!-- /form -->
</div>


 <!-- /container -->
</div>
</main>
</body>
</html>
