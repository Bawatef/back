<?php
require('database.php');
//$id = $_GET['id'];
//$client = $_GET['client'];
$sth = $con->prepare('SELECT * FROM ref WHERE id = :id');
$sth->execute(array(
    'id' =>$_GET['id'],
    ));
$results = $sth->fetchAll();
  //echo '<pre>';
  //var_dump($results);
  foreach($results as $item) {
      $id=$item['id'];
      $client = $item['client'];
      $context=$item['context'];
      $objective=$item['objective'];
      $presentation = $item['presentation'];
      $outputref = $item['outputref'];
      $createdAT = $item['createdAT'];

  }

//echo '</pre>';

 ?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="favicon.ico">

    <title>Références - client</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/narrow-jumbotron.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/main.css">
	  <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="css/styless.css" media="all">
</head>


<body>



    <header class="header clearfix"> <!-- header -->
        <h1 class="text-center"> Linagora Client's </h1>
    </header> <!-- /header -->


<main class = "main">
  <div class="nav">
    <div class="user">
    <?php
    session_start();
    echo "Bonjour ".$_SESSION['username'];
    ?>
  </div>
  <div class="deconnexion">
        <form  action= 'index.php'>
          <input type="submit"  value="Déconnexion">
        </form>
  </div>

  <div >
    <form  action="references.php">
            <input type="submit"  value="Retour">
    </form>
</div>

</div><!-- main -->

 <div class="table-responsive">
<table > <!-- table -->
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Client</th>
                <th scope="col">Context</th>
                <th scope="col">Objective</th>
                <th scope="col">Presentation</th>
                <th scope="col">Outputref</th>
                <th scope="col">CreatedAT</th>
            </tr>
            </thead>
<tbody><!-- table -->

     <tr>
         <td scope="col"> <?php echo $id; ?> </td>
         <td scope="col"> <?php echo $client; ?> </td>
         <td scope="col"> <?php echo $context; ?> </td>
         <td scope="col"> <?php echo $objective ; ?> </td>
         <td scope="col"> <?php echo $presentation ; ?> </td>
         <td scope="col"> <?php echo $outputref ; ?> </td>
         <td scope="col"> <?php echo $createdAT; ?>  </td>
     </tr>
 </tbody>
</table>
</div>


</main>
</body>
</html>
