<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="favicon.ico">

    <title>Références - Linagora</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/narrow-jumbotron.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" href="css/styless.css" media="all">
</head>

<body>
 <!-- container -->


    <header class="header">
        <h1 class="text-center"> Résultats de votre recherche </h1>
    </header> <!-- /header -->

  <main class = "main"> <!-- main -->
  <!-- affichage du nom d'utilisateur-->

  <div class="nav">
  <div class="user">
  <?php
  session_start();
  echo "Bonjour ".$_SESSION['username'];
  //}
  ?>
  </div>
  <form method="get" action="search.php" class="search">
          <input type="search" placeholder="Rechercher" name="clef" id="lookup" />
          <button type="submit" class="icone-loupe">Q</button>
  </form>

  <div class="deconnexion">
        <form  action= 'index.php'>
          <input type="submit"  value="Déconnexion">
        </form>
  </div>
  <div >
      <form  action="references.php">
              <input type="submit"  value="Retour">
      </form>
  </div>


  </div>

<?php
require('database.php');
//$id = $_GET['id'];
//$client = $_GET['client'];


if(!empty($_GET['clef'])){ // on vérifie d'abord l'existence du POST et aussi si la requete n'est pas vide.

$sth = $con->prepare('SELECT * FROM `ref` WHERE `client` LIKE :clef');
$search =  '%'.$_GET['clef'].'%';
$sth->execute(array('clef' => $search,));
$results = $sth->fetchAll(PDO::FETCH_ASSOC);
  //echo '<pre>';
  //var_dump($results);
  //echo '</pre>';
foreach($results as $value) {
  $search= $value['clef'] ;
  $id =$value['id'];
  $client = $value['client'];
  $context=$value['context'];
  $objective=$value['objective'];
  $presentation = $value['presentation'];
  $outputref = $value['outputref'];
  $createdAT = $value['createdAT'];
  }
  ?>

  <div class="table-responsive">
  <table > <!-- table -->
             <thead>
             <tr>
                 <th scope="col">ID</th>
                 <th scope="col">Client</th>
                 <th scope="col">Context</th>
                 <th scope="col">Objective</th>
                 <th scope="col">Presentation</th>
                 <th scope="col">Outputref</th>
                 <th scope="col">CreatedAT</th>
             </tr>
             </thead>
  <tbody><!-- table -->

      <tr>
          <td scope="col"> <?php echo $id; ?> </td>
          <td scope="col"> <?php echo $client; ?> </td>
          <td scope="col"> <?php echo $context; ?> </td>
          <td scope="col"> <?php echo $objective ; ?> </td>
          <td scope="col"> <?php echo $presentation ; ?> </td>
          <td scope="col"> <?php echo $outputref ; ?> </td>
          <td scope="col"> <?php echo $createdAT; ?>  </td>
      </tr>
  </tbody>
  </table>
  </div>

<?php } ; ?>

</main>
</body>
</html>
