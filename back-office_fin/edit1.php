<?php
require('select.php');

$id = $_GET['id'];

if(!empty($_POST['client']) && !empty($_POST['context']) && !empty($_POST['presentation']) && !empty($_POST['objective'])

&& !empty($_POST['outputref'])) {

  $client = $_POST['client'];
  $context=$_POST['context'];
  $presentation = $_POST['presentation'];
  $objective=$_POST['objective'];
  $outputref = $_POST['outputref'];
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="favicon.ico">

    <title>Références - Linagora</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/narrow-jumbotron.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/styless.css">
</head>

<body>
  <!-- container -->

      <header class="header clearfix"> <!-- header -->
          <h1 class="text-center"> Modifier la référence </h1>

      </header> <!-- /header -->

<main class="mainadd">
<!-- main -->
  <div class="nav">

    <div class="user">
    <?php
    session_start();
    echo "Bonjour ".$_SESSION['username'];
    ?>
    </div>

    <div class="deconnexion">
        <form  action= 'index.php'>
          <input type="submit"  value="Déconnexion">
        </form>
    </div>

  <div class="bouton">
    <form  action="edit.php">
      <div class="update">
        <input type="submit" value="Update">
      </div>
  </form>
</div>
<div>
 <form method="POST" class="form-horizontal" action="references.php">
      <div class="update" >
      	<input type="submit"  value="Retour">
      </div>
  </form>
 </div>

</div>

<!-- main -->

  <div class="row">
    <form method="POST" class="form-horizontal">
            <div class="col-lg-12">
                    <h4>Client</h4>
                    <input type="text" class='form-control' id='client' name='client' placeholder="Client" required value="<?= $client ?>">
            </div>

            <div class="col-lg-6">
                <h4>Context </h4>
                <textarea type="text" class='form-control' id='context' name='context' required> <?= $context ?></textarea>

                <h4>Project presentation </h4>
                <textarea type="text" class='form-control' id='presentation' name='presentation' required> <?= $presentation ?></textarea>
            </div>

            <div class="col-lg-6">
                <h4>Objective </h4>
                <textarea type="text" class='form-control' id='objective' name='objective' required><?= $objective ?></textarea>

                <h4>Output</h4>
                <textarea type="text" class='form-control' id='outputref' name='outputref' required><?= $outputref ?></textarea>
            </div>
  </form>
</div>


<?php

$update = $con->prepare("UPDATE `ref` SET `client`=:client , `context`=:context, `presentation`=:presentation, `objective`=:objective, `outputref`=:outputref WHERE `id`=:id");
$update->execute(array(
  'id' =>$id,
  'client' =>$client,
  'context' =>$context,
  'presentation' => $presentation,
  'objective' =>$objective,
  'outputref' =>$outputref,

  ));
?>


</main>
</body>
</html>
