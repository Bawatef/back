<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="favicon.ico">

    <title>Références - Linagora</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/narrow-jumbotron.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/styless.css">
</head>

<?php
require('database.php');
$sth = $con->prepare('DELETE FROM `ref` WHERE `id` = :id');
$sth->execute(array(
    'id' =>$_GET['id'],
    ));


//echo "<h2> L'élément a été correctement supprimé </h2>";
?>
<body>

    <header class="header"> <!-- header -->
        <h1 class="text-center"> Suppimer une référence </h1>
    </header>

<main class="maindel">

<div class='sup'>
  <p> La référence a été correctement supprimé </p>
</div>

<div class="update">

    <form  action="references.php" method= "get">
    <div class="col-lg-12">
        <input type="submit" value="Retour">
    </div>
    </form>

</div>


</main>
</body>
</html>
